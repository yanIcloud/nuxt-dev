# nuxt-dev

#### 介绍
主要为了提升家乡发展,达到共享,共知,开源的精神思想.

#### 软件架构
nuxt.js 采用SSR渲染方式


#### 安装教程

1. clone  **county-newest-project**  该项目
2. 安装node.js
3. cmd窗口进入 **county-newest-project** 文件下,  **npm install**    安装依赖.
4. 安装完成之后  执行:  **npm run dev**  启动该项目 默认端口 8089


#### author
dev:
mister_wei , mister_chen  
 
### 提交须知:
提交须在本级目录提交,切记不可提交到代码项目中.

