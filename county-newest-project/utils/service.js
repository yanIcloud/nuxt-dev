import axios from 'axios'
import store from '~/store'

const service = axios.create({
    baseURL: store().state.BASE_URL,
    timeout:30000
})

export default service