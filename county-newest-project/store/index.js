import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
const store =  ()=>{
    return new Vuex.Store({
          
        state:{   //开发环境的全局访问地址,如果是生产环境配置这个url即可
            BASE_URL:"http://127.0.0.1"
        }
        /* state:{
            BASE_URL:"http://192.168.22.15:8080"
        }*/
    })
} 

export default store