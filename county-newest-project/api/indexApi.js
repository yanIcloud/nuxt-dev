import request from '@/utils/service'

export default{
    findIndexList(){
         
        return request({
            url:`/findIndexList`,
            method:'get'
        })
    }
}